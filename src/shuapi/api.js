import Axios from 'axios';
import qs from 'qs';

Axios.defaults.timeout = 10000; //响应时间
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';           //配置请求头
// axios.defaults.baseURL = 'http://spsj.xn62x.cn';   //配置接口地址
// axios.defaults.baseURL = 'http://192.168.1.100';   //配置接口地址
// 添加一个响应拦截器
Axios.interceptors.request.use(
    config => {
        let account = sessionStorage.account
        let token = sessionStorage.token
        config.headers['token'] = token
        config.headers['account'] = account
        // 在发送请求之前做某件事
        // 判断是否登录
        // let cur_id = "cur_id",
        // sign = "sign";
        //  if (!cur_id||!sign) {
        //    localStorage.clear();
        //    window.location.href = "index.html";
        //  };

        if (config.method === 'post') {
            config.data = qs.stringify(config.data)
            if (config.url.indexOf('checkGoogleCommand') !== -1) {
                config.data = config.data
            } else if (config.url.indexOf('ti_bi') !== -1) {
                config.data = config.data
            } else {
                config.data = config.data + '&account=' + account
            }
        }
        if (config.url == Axios.send_sms) {
            config.data = config.data + '&mcode=' + sessionStorage.mcode
        }

        if (config.method === 'get') {
            config.params.account = account
        }
        return config
    },
    error => {
        _.toast('错误的传参', 'fail')
        return Promise.reject(error)
    }
)

// 添加返回信息验证
Axios.interceptors.response.use(
    function(response) {
        if (response.data.status == '505' || response.data.status == '404') {
            if (!sessionStorage.account) {
                return false
            }
            sessionStorage.clear()
            alert(response.data.msg)
            window.location.href = '/#/login'
        }
        return response
    },
    function(error) {
        return Promise.reject(error)
    }
);


let second = 'https://www.gccoin.pro'
Axios.bases = `${second}/`;
Axios.send_sms = `${second}/Home/Qbw/send_sms` // 短信验证码  1注册 2 重置 3 安全验证 4 支付密码设置 5 提币 mobile ,type

export default Axios